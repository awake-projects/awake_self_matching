"""
setup.py for awake-self-matching.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages
import os
import ast

MODULE_NAME = "awake_self_matching"


def read_version_from_init():
    """
    Extracts version from __init.py__. Inspired by PyJAPC.
    """
    init_file = os.path.join(
        os.path.dirname(__file__), MODULE_NAME, "__init__.py"
    )
    with open(init_file, "r") as file_handle:
        for line in file_handle:
            if line.startswith("__version__"):
                return ast.literal_eval(line.split("=", 1)[1].strip())

    raise Exception("Failed to extract version from __init__.py")


HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    "core": [
        "numpy==1.*",
        "pyjapc>=2.1.*",
        "matplotlib==3.*",
        "scipy==1.4.*",
        "tensorflow==2.*",
        "gym==0.17.*",
        "pandas",
        "cernml-coi",
        "awake_btv_vae",
        "cern-mlp-client",
    ],
    "test": [
        "pytest",
    ],
    "dev": [
        # 'requirement-for-development-purposes-only',
    ],
    "doc": [
        "sphinx",
        "acc-py-sphinx",
    ],
}


setup(
    name="awake-self-matching",
    version=read_version_from_init(),
    author="Francesco Maria Velotti",
    author_email="francesco.maria.velotti@cern.ch",
    description="SHORT DESCRIPTION OF PROJECT",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="",
    packages=find_packages(),
    python_requires=">=3.6, <4",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=REQUIREMENTS["core"],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        "dev": [
            req
            for extra in ["dev", "test", "doc"]
            for req in REQUIREMENTS.get(extra, [])
        ],
        # The 'all' extra is the union of all requirements.
        "all": [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    include_package_data=True,
)
