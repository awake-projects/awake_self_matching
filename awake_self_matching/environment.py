import numpy as np
import pyjapc
import typing as t
import logging.config
from gym import spaces
from awake_self_matching.data_helpers.mok_dataSource import (
    Btv as Btv_vae,
)
from awake_self_matching.data_helpers.dataSource import Btv as Btv_data
import time
from pathlib import Path
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from awake_self_matching.trained_models import trainedmodel

# from awake_self_matching.trained_models import encoder_model
from cernml.coi import OptEnv, register, Machine


class tlOptEnv(OptEnv):
    """
    The environment defines which actions can be taken at which point and
    when the agent receives which reward.
    """

    def __init__(
        self,
        japc=None,
        data_type="vae",
        state_type="explicit",
        state_dof=5,
        encoder_type="fixed",
        seed=0,
        action_space_type="sb",
        reward_dang=False,
        new_model={},
        no_plot=True,
        log_path={},
        btv_name="BTV54",
    ):
        self.path = Path(__file__).parent.absolute()
        self.max_episode_length = 100
        self.__version__ = "0.0.1"
        self.__name__ = "btvEnvironment"
        logging.info(
            "btvEnvironment - Version {}".format(self.__version__)
        )

        # Seed
        self.seed(seed)

        self.btv_name = btv_name
        self.plot_freq = 10
        self.save_freq = self.plot_freq

        self.devices = [
            "RPSKN.TSG4.SNH.430000",
            "RPSKN.TSG4.SNJ.430001",
            #         'RPCAH.TSG4.RCIBH.430011',
            #         'RPCAH.TSG4.RCIBV.430011',
            #         'RPCAH.TSG4.RCIBH.412353',
            #         'RPCAH.TSG4.RCIBV.412353',
            "logical.RQID.430031",
            "logical.RQIF.430034",
            "logical.RQID.430037",
        ]

        self.data_type = data_type
        self.state_type = state_type
        self.gamma = 1

        self.set_log_path(log_path)

        self.stats_train_log = {}

        self.now_str_all = (
            str(datetime.datetime.now())
            .replace(" ", "_")
            .replace(":", "_")
        )

        possible_action_spaces = ["su", "sb"]
        self.action_space_type = action_space_type
        if self.action_space_type not in possible_action_spaces:
            raise NameError(
                self.action_space_type
                + " is not in "
                + " ".join(possible_action_spaces)
            )

        possible_meas = ["vae", "btv", "4screens"]
        if self.data_type not in possible_meas:
            raise NameError(
                self.data_type + " is not in " + " ".join(possible_meas)
            )

        possible_encoders = ["fixed", "centred"]
        self.encoder_type = encoder_type
        if self.encoder_type not in possible_encoders:
            raise NameError(
                self.encoder_type
                + " is not in "
                + " ".join(possible_encoders)
            )

        if self.data_type == "vae":
            # self.japc = pyjapc.PyJapc('SPS.USER.ALL', noSet=True)
            self.japc = None
        else:
            if japc is None:
                japc = pyjapc.PyJapc("SPS.USER.ALL")

            self.japc = japc
            print("*******SETTING to HW!********")
            self.japc.rbacLogin()

        self.reward_dang = reward_dang
        if self.reward_dang:
            self.reward_dang_rate = 0.99
            self.reward_dang_min = -0.45
            self.reward_dang_max = -0.15

        self.no_beam_recorded = 0

        self.tf_sess = None
        self.dof = len(self.devices)
        # Limits [sole0, sole1, qd31, 34, 37]
        # for correctors, limits is +/- 10 A
        self.limits = [220, 220, 35, 35, 35]
        self.neg_limits = [0, 0, -35, -35, -35]

        # action space
        high = np.ones(self.dof)
        if self.action_space_type == "su":
            low = np.zeros(self.dof)
        else:
            low = -1 * np.ones(self.dof)
        self.action_space = spaces.Box(
            low=low, high=high, dtype=np.float32
        )
        self.optimization_space = self.action_space

        # state space
        if self.state_type == "explicit":
            self.state_dof = state_dof
            self.stateEncoder = new_model.get(
                "model",
                trainedmodel.TrainedModel(self.tf_sess, self.state_dof),
            )
        elif self.state_type == "implicit":
            if self.encoder_type == "fixed":
                self.state_dof = state_dof
                # Instantiate encode object from auto encoder
                self.stateEncoder = new_model.get(
                    "model",
                    trainedmodel.TrainedModel(
                        self.tf_sess, self.state_dof
                    ),
                )
            elif self.encoder_type == "centred":
                # TODO: make this dynamic
                self.state_dof = 5
                # Instantiate encode object from auto encoder
                self.stateEncoder = encoder_model.TrainedModel(
                    self.tf_sess, self.state_dof
                )

        high = np.ones(self.state_dof)
        low = np.ones(self.state_dof) * -1
        self.observation_space = spaces.Box(low, high, dtype=np.float32)

        # Minimum size achievable
        self.r0 = np.sqrt(0.25 ** 2 + 0.25 ** 2)

        # Max intensity
        self.i0 = 1.3e6

        # Max size
        self.r_max = np.sqrt(3 ** 2 + 3 ** 2)

        # reward sharing: fraction on beam size
        self.reward_fraction_sigma = 0.8

        # Reward threshold
        self.reward_target = -0.35

        self.curr_episode = -1
        self.action_episode_memory = []

        self._ini_buffer()

        self.curr_step = -1
        self.is_finalized = False
        self.counter = 0

        if self.data_type == "vae":
            # Instantiate Btv object from auto encoder
            self.model = Btv_vae(
                japc=self.japc,
                btv_name=self.btv_name,
                loaded_model=self.stateEncoder,
            )
        elif self.data_type == "btv":
            print("+++++++++ Getting real data ++++++++")
            self.model = Btv_data(
                japc=self.japc, btv_name=self.btv_name
            )
        elif self.data_type == "4screens":
            pass

        # Set initial conditions from working settings
        ini_file = pickle.load(
            open(
                self.path
                / "data_helpers"
                / "pickles"
                / "initial_para_last.pkl",
                "rb",
            )
        )
        ini_dic = {}
        for i, device in enumerate(self.devices):
            ini_dic[device] = [ini_file["values"][i], self.limits[i]]

        self.x0_scaling = 1
        self.ini_dic = ini_dic
        self.set_x0()

        self.rand_x0 = [0.1, 0.1, 0.2, 0.2, 0.2]

        self.off_set = self.x0

        # Lists for plotting all
        self.action_list = []
        self.state_list = []
        self.reward_list = []
        self.reward_xy = []
        self.reward_i = []

        # Init state
        self.state = np.zeros(self.state_dof)

        # Init action
        self.action_real = np.zeros_like(self.devices)
        self.action_scaling = 1.0

        # lists for plotting per episode
        self.reward_per_episode = {
            "intensity": {},
            "size": {},
            "total": {},
            "disc": [],
        }

        # Plotting
        if not no_plot:
            self.fig = plt.figure(1, figsize=(4, 8))
            self.fig.show()

            self.fig2 = plt.figure(2, figsize=(4, 6))
            self.fig2.show()

            self.fig3 = plt.figure(3, figsize=(4, 8))
            self.fig3.show()

            self.fig4 = plt.figure(4, figsize=(3, 2))
            self.fig4.show()

            self.fig5 = plt.figure(5, figsize=(4, 8))
            self.fig5.show()

        self.no_plot = no_plot
        # Save hyper parameters
        # and add additional parameters to be logged
        # Reward reset scaling: [0.01, 0.01, 0.01, 0.01, 0.01]
        # state_type, data_type, state_dof = "implicit", 'vae', 5
        # encoder_type = 'centred'
        self.additional_logging = {
            "x0_sigmas": self.rand_x0,
            "state_type": self.state_type,
            "data_type": self.data_type,
            "state_dof": self.state_dof,
            "encoder_type": self.encoder_type,
        }

    def _ini_buffer(self):
        self.replay_buffer = {
            "time": [],
            "penalty": [],
            "btv_data": [],
            "fault_state": [],
        }
        for dev in self.devices:
            self.replay_buffer[dev] = []
        # Adding states to logging
        for dev in range(self.state_dof):
            self.replay_buffer["s" + str(dev)] = []

    def set_x0(self):
        x0 = []
        constr_limits = []
        for device in self.devices:
            x0.append(self.ini_dic[device][0])
            constr_limits.append(self.ini_dic[device][1])
        self.x0 = self.normData(np.array(x0) * self.x0_scaling)

    def _is_there_beam(self):
        bpm1 = "TT43.BPM.430028/Acquisition#sigmaAvePos"
        data = self.japc.getParam(bpm1, timingSelectorOverride="")
        return data >= 100

    def set_log_path(self, log_path):

        if len(log_path) > 1:
            self.log_sett = log_path.get("sett", "data")
            self.log_stat = log_path.get("stat", "data")
            self.log_data = log_path.get("data", "data")
        else:
            self.log_sett = log_path.get("path", "data")
            self.log_stat = log_path.get("path", "data")
            self.log_data = log_path.get("path", "data")

    def seed(self, seed):
        np.random.seed(seed)

    # Method for generic optimisation framework
    def compute_single_objective(self, action):
        if self.curr_episode < 0:
            self._make_buffers()
        self.off_set = np.zeros(self.dof)
        _, reward, _, _ = self.step(action)

        return -reward

    def step(self, delta_action):
        """
        The agent takes a step in the environment.
        Parameters
        ----------
        action : int
        Returns
        -------
        observable, reward, episode_over, info : tuple
            observable (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
        # if self.is_finalized:
        #    raise RuntimeError("Episode is done")
        self.curr_step += 1
        self.counter += 1
        remaining_steps = self.max_episode_length - self.counter

        state, reward = self._take_action(delta_action)

        # Terminate episode if max episode length reached
        if self.counter > self.max_episode_length:
            self.is_finalized = True
        return state, reward, self.is_finalized, self.data_btv

    def save_data_to_file(self, user_filename=None):
        # Storing files in memory
        self.now = datetime.datetime.now()
        self.store()

        # Dumping files on disk
        if self.curr_step % self.save_freq == 0:
            print("Saving...")
            self.save_data(user_filename=user_filename)

    def _take_action(self, delta_action):

        self.delta_action = delta_action

        # Add action to off set => working in deltas!
        action = self.off_set + delta_action * self.action_scaling
        self.off_set = action

        self.action_ok, action_corrected = self.is_action_norm_ok(
            action
        )

        # Un-normalise data
        self.action_real = self.invNormData(action_corrected)
        self.action_real = self.action_real.reshape(1, -1)

        # Taking the real action on the system
        if self.data_type == "btv":
            while True:
                if self._is_there_beam():
                    self.set_actors(self.action_real)
                    break
                else:
                    time.sleep(1.2)
                    self.no_beam_recorded += 1
                    fault = self.japc.getParam(
                        "SR.AWINTLK/ExpertSt#level2Faults",
                        timingSelectorOverride="",
                    )
                    print("++++++ no beam recorded")
                    print("Faults lev2: ", fault)
                    try:
                        i_bend1 = self.japc.getParam(
                            "RPAAN.TSG4.RBIH.412343/MEAS.I.VALUE#value",
                            timingSelectorOverride="",
                        )
                        if not fault and i_bend1 > 1.0:
                            break
                    except Exception as e:
                        print(e)
                        if not fault and self._is_there_beam():
                            break

        # Getting reward from real action
        # self.reward = self._get_reward()
        if self.action_ok:
            self.reward = self._get_reward()
        else:

            self.reward = -1

        # Observe (get state)
        self._observe()

        if self.reward > self.reward_target and self.counter > 0:
            self.is_finalized = True
            if self.reward_dang:
                self.reward_target *= self.reward_dang_rate
                self.reward_target = np.clip(
                    self.reward_target,
                    self.reward_dang_min,
                    self.reward_dang_max,
                )

        return self.state, self.reward

    def get_actions(self):
        return self.action_real

    def _observe(self):
        # Make the agent aware of observables
        if self.state_type == "explicit":
            if self.state_dof == 3:
                self.state = [
                    np.clip((self.sx) / 8, 0, 1),
                    np.clip((self.sy) / 8, 0, 1),
                    -1 * self.intensity,
                ]
            elif self.state_dof == 5:
                self.state = [
                    np.clip((self.sx) / 8, 0, 1),
                    np.clip((self.sy) / 8, 0, 1),
                    np.clip(self.data_btv["mu_x"] / 10, 0, 1),
                    np.clip(-1 * self.data_btv["mu_y"] / 10, 0, 1),
                    -1 * self.intensity,
                ]
        elif self.state_type == "implicit":
            if self.encoder_type == "fixed":
                self.state = list(
                    self.stateEncoder.encodeImage(
                        self.data_btv["raw_image"]
                    )
                )
            elif self.encoder_type == "centred":
                self.state = list(
                    self.stateEncoder.encodeImage(
                        self.data_btv["raw_image"]
                    )
                )

        self.state = np.array(self.state)

        self.action_list.append(self.action_real[0])
        self.state_list.append(np.squeeze(self.state))
        self.reward_list.append(self.reward)
        self.reward_i.append(self.intensity)
        self.reward_xy.append(self.r)

        self.reward_per_episode["intensity"][self.curr_episode].append(
            self.intensity
        )
        self.reward_per_episode["size"][self.curr_episode].append(
            self.r
        )
        self.reward_per_episode["total"][self.curr_episode].append(
            self.reward
        )
        # Stop if reward target achieved

    def _get_reward(self):

        # Collecting data from BTV
        # self.sx, self.sy, self.data_btv = self.model.get_image_data(nShot=2, rms=False)
        if self.data_type == "vae":
            self.sx, self.sy, self.data_btv = self.model.get_image_data(
                nShot=1,
                rms=False,
                action=self.action_real,
                is_action_ok=self.action_ok,
            )
        elif self.data_type == "btv":
            self.sx, self.sy, self.data_btv = self.model.get_image_data(
                nShot=1, rms=False
            )

        self.sigmas = (self.sx, self.sy)
        self.intensity = (
            np.sum(self.data_btv["image"]) - self.i0
        ) / self.i0

        self.r = (
            -1
            * np.abs(
                self.r0 - np.sqrt(np.sum(np.array(self.sigmas) ** 2))
            )
            / self.r_max
        )

        self.intensity = np.clip(self.intensity, -1, 0)

        # Normalisation of reward as combination of beam size and intensity from BTV
        reward = (
            self.r * self.reward_fraction_sigma
            + self.intensity * (1 - self.reward_fraction_sigma)
        )

        return reward

    def _make_buffers(self):
        self.reward_per_episode["intensity"][self.curr_episode] = []
        self.reward_per_episode["size"][self.curr_episode] = []
        self.reward_per_episode["total"][self.curr_episode] = []

    def reset(self):
        """
        Reset the state of the environment and returns an initial observation.
        Returns
        -------
        observation (object): the initial observation of the space.
        """
        # Render stats per episode
        if self.curr_episode > -1:
            power = np.arange(
                len(self.reward_per_episode["total"][self.curr_episode])
            )
            gamma_in_time = self.gamma ** power
            self.reward_per_episode["disc"].append(
                np.sum(
                    gamma_in_time
                    * self.reward_per_episode["total"][
                        self.curr_episode
                    ]
                )
            )
            # self.store_stats()
            # self.save_stats()

        self.curr_episode += 1
        self.is_finalized = False
        self.counter = 0
        self.state = np.zeros_like(self.state)

        self._make_buffers()

        # Randomly initialise the off set
        self.off_set = self.x0 + np.array(
            self.rand_x0
        ) * np.random.randn(self.dof)

        state, _ = self._take_action(np.zeros(self.dof))
        return state

    def get_initial_params(self) -> t.Any:
        self.curr_episode += 1
        self._make_buffers()
        return self.x0

    def set_actors(self, state):
        # sole 1
        # sole 2
        # corr1 x, y
        # corr2 x, y
        # quad1
        # quad2
        # quad3
        for i, device in enumerate(self.devices):
            if "logical" in device:
                self.set_K(device, state[0][i])
            else:
                self.set_current(device, state[0][i])
        if not self.data_type == "vae":
            time.sleep(1.2)

    def set_virtual_parameter(self, device, value):
        ele = self.japc.getParam(
            device, noPyConversion=True, timingSelectorOverride=""
        ).getValue()
        ele.double = value
        self.japc.setParam(device, ele, timingSelectorOverride="")
        if not self.data_type == "vae":
            time.sleep(0.5)

    def set_current(self, device, value):
        self.japc.setParam(
            device + "/SettingPPM#current",
            value,
            timingSelectorOverride="",
        )

    def set_K(self, device, value):
        self.japc.setParam(
            device + "/K", value, timingSelectorOverride=""
        )

    def normData(self, x_data):
        x_data_norm = np.zeros(len(x_data))
        x_ext = np.zeros((len(x_data), 2))
        for i in range(len(x_data)):
            if self.action_space_type == "su":
                if self.limits[i] > 40:
                    x_ext[i, :] = [0, self.limits[i]]
                else:
                    x_ext[i, :] = [-self.limits[i], self.limits[i]]
            elif self.action_space_type == "sb":
                if self.limits[i] > 40:
                    x_data_norm[i] = (
                        2 * x_data[i] / self.limits[i]
                    ) - 1
                else:
                    x_data_norm[i] = x_data[i] / self.limits[i]
        if self.action_space_type == "su":
            for i in range(len(x_data)):
                x_data_norm[i] = (x_data[i] - x_ext[i, 0]) / (
                    x_ext[i, 1] - x_ext[i, 0]
                )
        return x_data_norm

    def invNormData(self, x_data):
        x_data_norm = np.zeros(len(x_data))
        x_ext = np.zeros((len(x_data), 2))
        for i in range(len(x_data)):
            if self.action_space_type == "su":
                if self.limits[i] > 40:
                    x_ext[i, :] = [0, self.limits[i]]
                else:
                    x_ext[i, :] = [-self.limits[i], self.limits[i]]
            elif self.action_space_type == "sb":
                if self.limits[i] > 40:
                    x_data_norm[i] = (
                        (x_data[i] + 1) * self.limits[i] / 2
                    )
                else:
                    x_data_norm[i] = x_data[i] * self.limits[i]
        if self.action_space_type == "su":
            for i in range(len(x_data)):
                x_data_norm[i] = (
                    x_data[i] * (x_ext[i, 1] - x_ext[i, 0])
                    + x_ext[i, 0]
                )
        return x_data_norm

    def is_action_norm_ok(self, x):
        if self.action_space_type == "su":
            norm_lim = (0, 1)
        else:
            norm_lim = (-1, 1)

        check = x > norm_lim[1]
        check2 = x < norm_lim[0]
        if any(check) or any(check2):
            x_clipped = np.clip(x, norm_lim[0], norm_lim[1])
            self.is_finalized = True
            return False, x_clipped
        else:
            return True, x

    def _render_per_interation(self, mode="human"):

        if not self.no_plot and mode == "mpl":
            if (
                self.curr_step % self.plot_freq == 0
                and not self.plot_freq == -1
            ):
                self.update_plots()
                self.update_plots2()
                self.update_plots4()
                self.update_plots5()
        elif mode == "human":
            print("------------------------------------------------")
            print("Iteration in episode: ", self.counter)
            print("Tot interaction: ", self.curr_step)
            print("Delta action: ", self.delta_action)
            print("Real action:", self.action_real)
            print(
                "State ("
                + str(self.state_dof)
                + "D "
                + self.state_type
                + "): ",
                self.state,
            )
            print("Reward: ", self.reward)
            print("------------------------------------------------")

    def update_all_plots(self):
        self.update_plots()
        self.update_plots2()
        self.update_plots3()
        self.update_plots4()
        self.update_plots5()

    def render(self, mode="human"):
        self._render_per_interation(mode)
        if "episode" in mode:
            self._render_per_episode(mode)

    def _render_per_episode(self, mode="human"):
        if not self.no_plot and mode == "mpl":
            self.update_plots3()
        if mode == "human":
            print("==================================================")
            print("Episode: ", self.curr_episode)
            print(
                "Episode length: ",
                len(self.reward_per_episode["size"][self.curr_episode]),
            )
            print("==================================================")
            for ele in ["size", "intensity", "total"]:
                print(
                    "Reward " + ele + " start: ",
                    self.reward_per_episode[ele][self.curr_episode][0],
                )
                print(
                    "Reward " + ele + " end: ",
                    self.reward_per_episode[ele][self.curr_episode][-1],
                )

            print(
                f'Disc rew tot: {self.reward_per_episode["disc"][self.curr_episode]}'
            )
            print("------------------------------------------------")

    def save_settings(self, additional_log):
        # Combining hyper-parameters and internal ones
        self.additional_logging = {
            **additional_log,
            **self.additional_logging,
        }
        # Dump on disk
        self.save_additional()

    def plot(self):
        data = pd.DataFrame(self.action_list)

        for i, col in enumerate(data.columns):
            a = self.fig.add_subplot(len(data.columns) + 1, 1, i + 1)
            a.plot(data[col])
            a.axhline(self.limits[i], ls="--", color="r")
            a.axhline(self.neg_limits[i], ls="--", color="r")
            a.set_ylabel("X$_%d$" % i)

        a.set_xlabel("Iterations")

    def update_plots(self):
        self.fig.clf()
        self.plot()
        self.fig.canvas.draw()
        self.fig.tight_layout()
        plt.pause(0.01)

    def plot2(self):
        data = self.data_btv

        a21 = self.fig2.add_subplot(211)

        a21.plot(
            data["x"],
            data["pro_x"],
            "r",
            label="X = %.2f mm" % data["sigma_x"],
        )
        a21.plot(data["x"], data["fit_x"], "--r")

        a21.plot(
            data["y"],
            data["pro_y"],
            "k",
            label="Y = %.2f mm" % data["sigma_y"],
        )
        a21.plot(data["y"], data["fit_y"], "--k")

        a21.legend()
        a21.set_ylabel("Amplitude")
        a21.set_xlabel("x, y / mm")

        a22 = self.fig2.add_subplot(212)
        a22.plot(
            np.array(self.reward_list),
            "-o",
            color="k",
            label="total",
            alpha=0.25,
        )
        a22.plot(self.reward_i, c="b", label="Intensity")
        a22.plot(
            self.reward_xy,
            "r.",
            label=r"$\sqrt{\sigma_x^2 + \sigma_y^2}$",
        )
        a22.set_ylim(-1.0, 0.2)
        a22.axhline(self.reward_target, ls="--")
        a22.set_xlabel("Iterations")
        a22.set_ylabel("Reward")
        #        a.set_yscale('log')
        a22.legend()

    def update_plots2(self):
        self.fig2.clf()
        self.plot2()
        self.fig2.canvas.draw()
        self.fig2.tight_layout()
        plt.pause(0.01)

    def plot3(self):
        data = self.reward_per_episode

        int_start = [
            data["intensity"][i][0]
            for i in range(1, self.curr_episode + 1)
        ]
        int_end = [
            data["intensity"][i][-1]
            for i in range(1, self.curr_episode + 1)
        ]

        sizes_start = [
            data["size"][i][0] for i in range(1, self.curr_episode + 1)
        ]
        sizes_end = [
            data["size"][i][-1] for i in range(1, self.curr_episode + 1)
        ]

        lengths = [
            len(data["size"][i])
            for i in range(1, self.curr_episode + 1)
        ]

        a = self.fig3.add_subplot(411)
        a.plot(
            sizes_start,
            label=r"initial $\sqrt{\sigma_x^2 + \sigma_y^2}$",
        )
        a.plot(
            sizes_end, label=r"final $\sqrt{\sigma_x^2 + \sigma_y^2}$"
        )
        a.legend()
        a.set_ylim(-2.0, 0.0)
        a.set_ylabel(r"$\sqrt{\sigma_x^2 + \sigma_y^2}$ / mm")

        a12 = self.fig3.add_subplot(412)
        a12.plot(int_start, label="Intensity initial")
        a12.plot(int_end, label="Intensity final")
        a12.legend()
        a12.set_ylabel("Intensity (a. u.)")

        a2 = self.fig3.add_subplot(413)
        a2.plot(lengths)
        a2.set_xlabel("Episodes")
        a2.set_ylabel("Episode len")

        a3 = self.fig3.add_subplot(414)
        a3.plot(self.reward_per_episode["disc"])
        a3.set_xlabel("Episodes")
        a3.set_ylabel("Discounted rew")

    def update_plots3(self):
        self.fig3.clf()
        self.plot3()
        self.fig3.canvas.draw()
        self.fig3.tight_layout()
        plt.pause(0.01)

    def plot4(self):
        data = self.data_btv
        a = self.fig4.add_subplot(111)
        X, Y = np.meshgrid(data["x"], data["y"])
        a.contourf(X, Y, data["image"])

        a.set_xlabel("x / mm")
        a.set_ylabel("y / mm")

    def update_plots4(self):
        self.fig4.clf()
        self.plot4()
        self.fig4.canvas.draw()
        self.fig4.tight_layout()
        plt.pause(0.01)

    def plot5(self):
        data = pd.DataFrame(self.state_list)
        n_cols = 7 if len(data.columns) > 7 else len(data.columns)

        for i, col in enumerate(data.columns[:n_cols]):
            a5 = self.fig5.add_subplot(n_cols, 1, i + 1)
            a5.plot(data[col])
            a5.set_ylabel(f"O$_{i}$")

        a5.set_xlabel("Iterations")

    def update_plots5(self):
        self.fig5.clf()
        self.plot5()
        self.fig5.canvas.draw()
        self.fig5.tight_layout()
        plt.pause(0.01)

    def save_data(self, user_filename=None):
        file_name = [
            self.log_data,
            "/settings_",
            self.now_str_all,
            "_",
            self.data_type,
            "_",
            self.state_type,
            ".p",
        ]
        if user_filename is not None:
            file_name = user_filename
        else:
            file_name = "".join(file_name)
        try:
            pickle.dump(
                self.replay_buffer,
                open(file_name, "wb"),
                pickle.HIGHEST_PROTOCOL,
            )

            # data_to_save = pd.DataFrame(self.replay_buffer)
            # data_to_save.to_hdf(
            #     "".join(file_name), key="data", mode="a"
            # )

        except Exception as e:
            print("++++++++++ error saving +++++++++")
            print(e)
            # quit()

    def store(self):
        self.replay_buffer["time"].append(self.now)
        self.replay_buffer["btv_data"].append(self.data_btv)
        self.replay_buffer["penalty"].append(np.abs(self.r))
        self.replay_buffer["fault_state"].append(self.no_beam_recorded)
        for i, dev in enumerate(self.devices):
            self.replay_buffer[dev].append(
                np.squeeze(self.action_real)[i]
            )

        for i in range(self.state_dof):
            self.replay_buffer["s" + str(i)].append(self.state[i])

    def store_stats(self):
        self.stats_train_log[self.curr_episode] = {}
        for ele in self.reward_per_episode.keys():

            self.stats_train_log[self.curr_episode][
                ele
            ] = self.reward_per_episode[ele][self.curr_episode]

    def save_stats(self):

        pickle.dump(
            self.stats_train_log,
            open(
                self.log_stat
                + "/train_stats_"
                + self.now_str_all
                + "_"
                + self.data_type
                + "_"
                + self.state_type
                + str(self.state_dof)
                + ".p",
                "wb",
            ),
        )

    def save_additional(self):

        pickle.dump(
            self.additional_logging,
            open(
                self.log_sett
                + "/data_scan_"
                + self.now_str_all
                + "_"
                + self.data_type
                + "_"
                + self.state_type
                + str(self.state_dof)
                + ".p",
                "wb",
            ),
        )


class DFAmatchingEnv(tlOptEnv):
    metadata = {
        "render.modes": ["human", "mpl"],
        "cern.machine": Machine.SPS,
        "cern.japc": False,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


register(
    "DFAmatchingEnv-v0",
    entry_point=DFAmatchingEnv,
    kwargs={"data_type": "vae"},
)


class BTVAmatchingEnv(tlOptEnv):
    metadata = {
        "render.modes": ["human", "mpl"],
        "cern.machine": Machine.SPS,
        "cern.japc": True,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


register(
    "BTVAmatchingEnv-v0",
    entry_point=BTVAmatchingEnv,
    kwargs={"data_type": "btv"},
)
