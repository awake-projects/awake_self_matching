import numpy as np
from . import frame_analysis as fa
import os
import pickle

file_path = os.path.dirname(os.path.abspath(__file__)) + "/pickles/"


class Btv:
    def __init__(self, btv_name, japc, loaded_model):
        self.btv_name = btv_name
        self.japc = japc
        if "BTV54" == self.btv_name:
            self.image_source = "BOVWA.11TCC4.AWAKECAM11/CameraImage"
            self.acquisition = "#image"

            self.height = "#height"
            self.width = "#width"
            self.p = "#pixelSize"
        elif "BTV42" == self.btv_name:
            self.image_source = "BOVWA.02TCV4.CAM9/CameraImage"
            self.acquisition = "#image"

            self.height = "#height"
            self.width = "#width"
            self.p = "#pixelSize"

        else:
            self.acquisition = "/Image#imageSet"
            self.axis1 = "/Image#imagePositionSet1"
            self.axis2 = "/Image#imagePositionSet2"
            self.height = "/Image#nbPtsInSet1"
            self.width = "/Image#nbPtsInSet2"

        self.model_btv = loaded_model
        self.penalty = 0
        self.image_all = None
        #        self._ini_image = self._get_image()
        self._ini_image = self.model_btv.imageFromC(
            np.zeros(5), is_action_ok=False
        )

        self._get_image_axis(load=True)
        self.roi = self._calc_roi()

        self.frame_ana = fa.FrameAna(
            self._ini_image, self.im_ax1, self.im_ax2, self.roi
        )
        self.frame_ana.fit_gauss = True
        self.frame_ana.analyze_frame()

    def _calc_roi(self):

        pro_x = self._ini_image.sum(axis=1)
        pro_y = self._ini_image.sum(axis=0)

        #        x_min = self.im_ax1[np.argmax(pro_x)] - 20
        #        x_max = self.im_ax1[np.argmax(pro_x)] + 20
        #        y_min = self.im_ax2[np.argmax(pro_y)] - 20
        #        y_max = self.im_ax2[np.argmax(pro_y)] + 20

        x_min = -1
        x_max = 14
        y_min = -14
        y_max = 4

        return np.array([x_min, x_max, y_min, y_max])

    def _get_image(self):
        image = self.model_btv.imageFromC(
            self.action_to_take, self.is_action_ok
        )
        self.valid_image = self._is_good_image(image)
        return image

    def _get_image_axis(self, load=False, dump=False):
        if load:
            if "BOVWA" in self.image_source:
                self.w = pickle.load(
                    open(
                        file_path
                        + self.image_source.replace("/", "_")
                        + self.height
                        + ".pkl",
                        "rb",
                    )
                )
                self.h = pickle.load(
                    open(
                        file_path
                        + self.image_source.replace("/", "_")
                        + self.width
                        + ".pkl",
                        "rb",
                    )
                )
            else:
                self.h = pickle.load(
                    open(
                        file_path
                        + self.image_source
                        + self.height.replace("/", "_")
                        + ".pkl",
                        "rb",
                    )
                )
                self.w = pickle.load(
                    open(
                        file_path
                        + self.image_source
                        + self.width.replace("/", "_")
                        + ".pkl",
                        "rb",
                    )
                )
            self.pix_size = pickle.load(
                open(
                    file_path
                    + self.image_source.replace("/", "_")
                    + "_pixel.pkl",
                    "rb",
                )
            )
            self.im_ax1, self.im_ax2 = pickle.load(
                open(
                    file_path
                    + self.image_source.replace("/", "_")
                    + "_axis.pkl",
                    "rb",
                )
            )
        else:
            if "BOVWA" in self.image_source:
                self.w = self.japc.getParam(
                    self.image_source + self.height,
                    timingSelectorOverride="",
                )
                self.h = self.japc.getParam(
                    self.image_source + self.width,
                    timingSelectorOverride="",
                )
                if self.btv_name == "BTV54":
                    self.pix_size = 0.134 / 5.0
                else:
                    self.pix_size = self.japc.getParam(
                        self.image_source + self.p,
                        timingSelectorOverride="",
                    )
                self.im_ax1 = (
                    np.linspace(-self.h / 2, self.h / 2, self.h)
                    * self.pix_size
                    * 5.0
                )
                self.im_ax2 = (
                    np.linspace(-self.w / 2, self.w / 2, self.w)
                    * self.pix_size
                    * 5.0
                )

            else:
                self.im_ax1 = self.japc.getParam(
                    self.image_source + self.axis1,
                    timingSelectorOverride="",
                )
                self.im_ax2 = self.japc.getParam(
                    self.image_source + self.axis2,
                    timingSelectorOverride="",
                )
                self.h = self.japc.getParam(
                    self.image_source + self.height,
                    timingSelectorOverride="",
                )
                self.w = self.japc.getParam(
                    self.image_source + self.width,
                    timingSelectorOverride="",
                )

                self._ini_image = np.reshape(
                    self._ini_image, (self.w, self.h)
                )
        if dump:
            if "BOVWA" in self.image_source:
                pickle.dump(
                    self.w,
                    open(
                        file_path
                        + self.image_source.replace("/", "_")
                        + self.height
                        + ".pkl",
                        "wb",
                    ),
                )
                pickle.dump(
                    self.h,
                    open(
                        file_path
                        + self.image_source.replace("/", "_")
                        + self.width
                        + ".pkl",
                        "wb",
                    ),
                )
            else:
                pickle.dump(
                    self.h,
                    open(
                        file_path
                        + self.image_source
                        + self.height.replace("/", "_")
                        + ".pkl",
                        "wb",
                    ),
                )
                pickle.dump(
                    self.w,
                    open(
                        file_path
                        + self.image_source
                        + self.width.replace("/", "_")
                        + ".pkl",
                        "wb",
                    ),
                )
            pickle.dump(
                self.pix_size,
                open(
                    file_path
                    + self.image_source.replace("/", "_")
                    + "_pixel.pkl",
                    "wb",
                ),
            )
            pickle.dump(
                (self.im_ax1, self.im_ax2),
                open(
                    file_path
                    + self.image_source.replace("/", "_")
                    + "_axis.pkl",
                    "wb",
                ),
            )

    def _get_multiple_images(self, nShot):
        self.image_all = None

        for count in range(nShot):
            self.raw_image = self._get_image()
            if self.valid_image:
                self.penalty = 0
            else:
                self.penalty = 1
            if "BOVWA" not in self.image_source:
                self.raw_image = np.reshape(
                    self.raw_image, (self.w, self.h)
                )

            self.frame_ana = fa.FrameAna()
            self.frame_ana.frame = self.raw_image
            self.frame_ana.x_ax = self.im_ax1
            self.frame_ana.y_ax = self.im_ax2

            self.frame_ana.roi = self.roi

            self.frame_ana.fit_gauss = True
            self.frame_ana.analyze_frame()

            if self.image_all is None:

                self.image_all = self.frame_ana.frame
            else:
                self.image_all = self.frame_ana.frame + self.image_all

            self.x_prof_array[count, :] = self.frame_ana.proj_x
            self.y_prof_array[count, :] = self.frame_ana.proj_y
            self.x_fit_array[count, :] = self.frame_ana.fit_x
            self.y_fit_array[count, :] = self.frame_ana.fit_y
            self.x_rms[count] = self.frame_ana.xRMS
            self.y_rms[count] = self.frame_ana.yRMS

            self.x_sigma[count] = self.frame_ana.sig_x
            self.y_sigma[count] = self.frame_ana.sig_y
            self.x_bar[count] = self.frame_ana.xBar
            self.y_bar[count] = self.frame_ana.yBar
            self.x_mu[count] = self.frame_ana.mean_x
            self.y_mu[count] = self.frame_ana.mean_y
        #            time.sleep(0.2)
        return (
            np.mean(self.x_rms) + self.penalty,
            np.mean(self.y_rms) + self.penalty,
            np.mean(self.x_sigma) + self.penalty,
            np.mean(self.y_sigma) + self.penalty,
        )

    def _ini_data_array(self, nShot):
        self.x_prof_array = np.zeros(
            (nShot, self.frame_ana.frame.shape[1])
        )
        self.y_prof_array = np.zeros(
            (nShot, self.frame_ana.frame.shape[0])
        )

        self.x_fit_array = np.zeros(
            (nShot, self.frame_ana.frame.shape[1])
        )
        self.y_fit_array = np.zeros(
            (nShot, self.frame_ana.frame.shape[0])
        )

        self.x_rms = np.zeros(nShot)
        self.y_rms = np.zeros(nShot)

        self.x_bar = np.zeros(nShot)
        self.y_bar = np.zeros(nShot)

        self.x_sigma = np.zeros(nShot)
        self.y_sigma = np.zeros(nShot)

        self.x_mu = np.zeros(nShot)
        self.y_mu = np.zeros(nShot)

    def _is_good_image(self, image):
        p_x = image.sum(axis=1)
        p_y = image.sum(axis=0)

        mean_x = np.mean(p_x)
        mean_y = np.mean(p_y)

        if (np.max(p_x) - mean_x) > 1.4 * mean_x and (
            np.max(p_y) - mean_y
        ) > 1.4 * mean_y:
            return True
        else:
            return False

    def _make_bi_gauss_fit(self):
        pass

    def get_image_data(
        self, nShot, rms=False, action=None, is_action_ok=True
    ):
        """
        Get N BTV data
        """
        self.is_action_ok = is_action_ok
        self.action_to_take = action
        self._ini_image = self._get_image()

        self._get_image_axis(load=True)
        self.roi = self._calc_roi()

        self.frame_ana = fa.FrameAna(
            self._ini_image, self.im_ax1, self.im_ax2, self.roi
        )
        self.frame_ana.fit_gauss = True
        self.frame_ana.analyze_frame()

        self._ini_data_array(nShot)

        xr, yr, xs, ys = self._get_multiple_images(nShot)

        # Save raw image
        # 64x64 around the mean
        data_btv_plotting = {
            "x": self.frame_ana.x_ax,
            "pro_x": np.mean(self.x_prof_array, axis=0),
            "y": self.frame_ana.y_ax,
            "pro_y": np.mean(self.y_prof_array, axis=0),
            "fit_x": np.mean(self.x_fit_array, axis=0),
            "fit_y": np.mean(self.y_fit_array, axis=0),
            "rms_x": np.mean(self.x_rms),
            "rms_y": np.mean(self.y_rms),
            "sigma_x": np.mean(self.x_sigma),
            "sigma_y": np.mean(self.y_sigma),
            "mu_x": np.mean(self.x_mu),
            "mu_y": np.mean(self.y_mu),
            "image": self.image_all,
            "raw_image": self.raw_image,
        }

        if rms:
            return xr, yr, data_btv_plotting
        else:
            return xs, ys, data_btv_plotting

    def _gaussian(height, center_x, center_y, width_x, width_y):
        """Returns a gaussian function with the given parameters"""
        width_x = float(width_x)
        width_y = float(width_y)
        return lambda x, y: height * np.exp(
            -(
                ((center_x - x) / width_x) ** 2
                + ((center_y - y) / width_y) ** 2
            )
            / 2
        )

    def _moments(data):
        """Returns (height, x, y, width_x, width_y)
        the gaussian parameters of a 2D distribution by calculating its
        moments"""
        total = data.sum()
        X, Y = np.indices(data.shape)
        x = (X * data).sum() / total
        y = (Y * data).sum() / total
        col = data[:, int(y)]
        width_x = np.sqrt(
            np.abs((np.arange(col.size) - y) ** 2 * col).sum()
            / col.sum()
        )
        row = data[int(x), :]
        width_y = np.sqrt(
            np.abs((np.arange(row.size) - x) ** 2 * row).sum()
            / row.sum()
        )
        height = data.max()
        return height, x, y, width_x, width_y
