"""
High-level tests for the  package.

"""

import awake_self_matching
from awake_self_matching.environment import tlOptEnv
from cernml.coi import check_env


def test_version():
    assert awake_self_matching.__version__ is not None


env = tlOptEnv()


def test_env():
    assert env is not None


def test_ini():
    ini_cond = env.get_initial_params()
    assert len(ini_cond) > 0


def test_penalty():
    penalty_test = env.compute_single_objective(env.x0)
    assert penalty_test >= 0


def check_cern_env():
    check_env(env)
